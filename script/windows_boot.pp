# Download and install GeoTrust

$ssh_dir="C:/Users/Administrator/.ssh"
$powershell="C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe"
$unix_bin="\"C:/Program Files (x86)/Git/bin\""
$puppetfile="C:/ProgramData/Puppetlabs/puppet/etc/Puppetfile"

package { 'r10k' : provider=>gem }->
package { 'git' :  provider=>chocolatey }->
file { "$ssh_dir" :ensure => directory }->
exec { 'get code-builder.pem':
	command =>  "Read-S3Object -BucketName keys-ams-vector -Key code-builder.pem -File $ssh_dir/code-builder.pem",
	onlyif => "If (Test-Path $ssh_dir/code-builder.pem) {Exit 1}",
	provider => powershell
}->
exec { 'keyscan bitbucket.org':
	command =>  "$unix_bin/ssh-keyscan -t rsa -T 60 bitbucket.org > $ssh_dir/known_hosts",
	onlyif => "If (Get-Content $ssh_dir/known_hosts | Select-String -Pattern bitbucket.org) {Exit 1}",
	provider => powershell
}->
exec { 'keyscan github.com':
	command =>  "$unix_bin/ssh-keyscan -t rsa -T 60 bitbucket.org >> $ssh_dir/known_hosts",
	onlyif => "If (Get-Content $ssh_dir/known_hosts | Select-String -Pattern github.com) {Exit 1}",
	provider => powershell
}->
file { "$ssh_dir/config":
	content => 
'Host bitbucket.org
Hostname bitbucket.org
IdentityFile ~/.ssh/code-builder.pem
User git
IdentitiesOnly yes

Host github.com
Hostname github.com
IdentityFile ~/.ssh/code-builder.pem
User git
IdentitiesOnly yes'
}->
file { $puppetfile :
  content => 
"forge 'https://forgeapi.puppetlabs.com'

mod 'apitalent-puppet_main',
  :git => 'git@bitbucket.org:apitalent/puppet_main.git'
"
}->
file_line { 'role' : 
	path => "$puppet_home/Puppetfile",
	line => "mod 'apitalent-$::puppet_role',
  :git => 'git@bitbucket.org:apitalent/puppet_modules.git',
  :path => 'modules/$::puppet_role'",
}
