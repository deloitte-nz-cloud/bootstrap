$choco="C:\ProgramData\chocolatey\bin\choco.exe"
#$manifest="C:\ProgramData\PuppetLabs\puppet\etc\windows_boot.pp"
$manifest="C:\\Users\Administrator\\Documents\\windows_boot.pp"
$puppet="C:\Program Files\Puppet Labs\Puppet\bin\puppet.bat"
$r10k="C:\Program Files\Puppet Labs\Puppet\sys\ruby\bin\r10k.bat"
$etc="C:\ProgramData\PuppetLabs\puppet\etc"
$configure="C:\ProgramData\PuppetLabs\puppet\etc\modules\puppet_main\manifest\configure_windows.pp"
$manifest_source='https://bitbucket.org/apitalent/bootstrap/src/7c4489e13e890d9d42d9b509039dbfa5f9ab6ac6/script/windows_boot.ps1'
$cert="C:\Users\Administrator\Documents\GeoTrust_Global_CA.pem"

Set-ExecutionPolicy Unrestricted -Scope Process -Force

# Install Chocolatey
iex ((new-object net.webclient).DownloadString('http://chocolatey.org/install.ps1'))

# Install puppet
& $choco install -y puppet

(new-object System.Net.WebClient).DownloadFile('https://www.geotrust.com/resources/root_certificates/certificates/GeoTrust_Global_CA.pem', $cert)
certutil -addstore Root $cert
& $puppet module install puppetlabs-stdlib
& $puppet module install puppetlabs-powershell
& $puppet module install puppetlabs-chocolatey

#(new-object System.Net.WebClient).DownloadFile($manifest_source, $manifest)
& $puppet apply -v $manifest

cd $etc
& $r10k puppetfile install --verbose

& $puppet apply -v $configure
& $r10k puppetfile install --verbose
& $puppet apply -ve 'include $::puppet_role'
