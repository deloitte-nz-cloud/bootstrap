#!/bin/bash

set -euo pipefail

usage()
{
   echo 'Usage: '"$(basename $0)"' -r <Server Role> -t <AWS Environment> -b S3 <Bucket> -k S3 <Key> -h <Git Host> -a <Git Account> -g <Git Repository> [-e <Puppet Environment>] [-s <Signal URL>] [-l latest]'
}

if (( $# < 14 || $# > 20 )); then
  usage
  exit 1
fi

echo Called with "$@"

while getopts "r:t:b:k:h:a:g:e:s:l" opt; do
  case $opt in
    r)
      puppet_role=$OPTARG
      ;;
    t)
      aws_environment=$OPTARG
      ;;
    b)
      s3_bucket=$OPTARG
      ;;
    k)
      s3_key=$OPTARG
      s3_file=$(basename $s3_key)
      ;;
    h)
      git_host=$OPTARG
      ;;
    a)
      git_account=$OPTARG
      ;;
    g)
      git_repo=$OPTARG
      ;;
    e)
      environment=$OPTARG
      ;;
    s)
      signal_url=$OPTARG
      ;;
    l)
      latest=y
      ;;
    *)
      echo "Invalid option: -$OPTARG" >&2
      usage
      exit 1
      ;;
  esac
done

environment=${environment:-production}
latest=${latest:-n}

fact_dir=/etc/facter/facts.d
fact_path=/etc/facter/facts.d/launch_puppet.txt
mkdir -p $fact_dir
cat >$fact_path <<EOF
puppet_role=$puppet_role
aws_environment=$aws_environment
s3_bucket=$s3_bucket
s3_key=$s3_key
s3_file=$s3_file
git_host=$git_host
git_account=$git_account
git_repo=$git_repo
environment=${environment:=}
signal_url=${signal_url:=}
latest=${latest:=}
EOF

if [[ $latest = y ]]; then
  echo "Latest puppet will be installed, note that installing from a third-party repo may cause yum conflicts in the future."
fi

yum_install()
{
  if ! yum list installed $1 &>/dev/null 2>&1; then
    yum install -y ${2:-$1} ${3:-}
  else
    yum upgrade -y ${2:-$1} ${3:-}
  fi
}

yum_erase()
{
  if yum list installed $1 &>/dev/null 2>&1; then
    yum erase -y ${2:-$1} ${3:-}
  fi
}

# $1 gem $2 check ver $3 install ver
gem_install()
{
  check_ver=${2:-}
  if [[ ${3:-} == latest ]]; then
    install_ver=
  else
    install_ver=$check_ver
  fi
  if [[ $(echo "$ruby_ver >= 1.9" | bc) == 1 ]]; then
    opts='-VN'
  else
    opts='-V --no-ri --no-rdoc'
  fi 
  re="$1.*${check_ver//./\.}"
  if ! echo $gem_list | egrep -q $re; then
    gem install $opts $1 ${install_ver:+-v}${install_ver:-}
  else
    if [[ -n $install_ver ]]; then
      gem uninstall -VIx $1
      gem install $opts $1 ${install_ver:+-v}${install_ver:-}
    else
      gem update $opts $1
    fi
  fi
}

yum_install ruby
yum_install rubygems

re='[[:digit:]]\.[[:digit:]]'
[[ "$(ruby --version)" =~ $re ]] && ruby_ver=${BASH_REMATCH[0]}
gem_list=$(gem list --local)

yum_install gcc
yum_install ruby-devel
if [[ $(echo "$ruby_ver >= 1.9" | bc) == 1 ]]; then
  gem_install puppet '3.7.5'
  gem_install librarian-puppet '2.1.0'
  gem_install io-console '0.4.2'
  echo forge_api_ver=3 >>$fact_path
else
  gem_install highline 1.6.21
  gem_install librarian-puppet 1.4.1
  echo forge_api_ver=1 >>$fact_path
fi

if [[ $latest = y ]]; then
  yum_install puppetlabs-release http://yum.puppetlabs.com/puppetlabs-release-el-6.noarch.rpm
  if [[ -r /etc/yum/pluginconf.d/priorities.conf ]] &&\
    yum_erase ruby18-augeas
    grep -q 'enabled = 1' /etc/yum/pluginconf.d/priorities.conf &&\
    ! grep -q 'priority=0' /etc/yum.repos.d/puppetlabs.repo; then
    sed -i '/[.*]/i\\priority=0' /etc/yum.repos.d/puppetlabs.repo
  fi
  if ! grep 'timeout=60' /etc/yum.conf; then
    echo timeout=60 >> /etc/yum.conf
  fi
  if yum search ruby18 | fgrep -q 'ruby18.'; then
    yum_install puppet puppet --exclude=rubygem-json
    yum_erase rubygem-json
    yum_install rubygem18-json
    for script in /usr/bin/{puppet,facter,hiera}; do
      sed  -i 's@^#!/usr/bin/ruby$@#!/usr/bin/ruby1.8@' $script
    done
  else
    yum_install puppet
  fi
else
  yum_erase puppetlabs-release
  yum_erase puppet
  yum_erase facter
  yum_install puppet
fi

# Download Bit Bucket key
if ! yum list aws-cli &>/dev/null; then
  if ! type pip &>/dev/null; then
    cd /tmp
    curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
    python get-pip.py    
  fi
  yum_install python-devel
  pip install awscli
fi
cd ~/.ssh
aws s3 cp s3://$s3_bucket/$s3_key .
chmod 600 $s3_file

cat >~/.ssh/config <<EOF
Host $git_host
Hostname $git_host
IdentityFile ~/.ssh/$s3_file
User git
IdentitiesOnly yes

Host github.com
Hostname github.com
IdentityFile ~/.ssh/$s3_file
User git
IdentitiesOnly yes
EOF

# Add Bitbucket Fingerprint
ssh-keyscan -t rsa -T 60 $git_host > ~/.ssh/known_hosts
ssh-keyscan -t rsa -T 60 github.com >> ~/.ssh/known_hosts

# Download puppet code
yum_install git
cd /etc/puppet
git archive --remote=git@$git_host:$git_account/$git_repo $environment | tar --overwrite -xvf -

# Install stdlibs to read facts from facts.d for old facter
re='[[:digit:]]\.[[:digit:]]'
[[ "$(facter --version)" =~ $re ]] && facter_ver=${BASH_REMATCH[0]}
if [[ $(echo "$facter_ver <= 1.6" | bc) == 1 ]] && ! puppet module list | grep -q puppetlabs-stdlib; then
    mkdir -p /etc/puppet/modules
    puppet module install puppetlabs-stdlib
fi

# Apply puppet configure
puppet apply -v /etc/puppet/manifests/configure.pp

# Install puppet modules
cd /etc/puppet
PATH=$PATH:/usr/local/bin
export HOME=${HOME:-~}
librarian-puppet install
# Apply puppet role
retval=0
puppet apply --detailed-exitcodes -ve 'include $::puppet_role' || retval=$?

case $retval in
  1|4) status=FAILURE
     data="There were failures during the puppet transaction"
     reason="Check the puppet logs"
     ;;
  6) status=FAILURE
     data="There were both changes and failures during the puppet transaction"
     reason="Check the puppet logs"
     ;;
  2) status=SUCCESS
     data="There were changes made during the puppet transaction"
     reason="Check the puppet logs"
     ;;
  0) status=SUCCESS
     data="There were no changes made during the puppet transaction"
     reason="Check the puppet logs"
     ;;
  *) status=FAILURE
     data="Unrecognised puppet return code $retval"
     reason="Check the puppet logs"
     ;;
esac

echo "$status : $data : $reason"

# Signal success
if [[ -n ${signal_url=} ]]; then
  curl -X PUT -H 'Content-Type:' \
  --data-binary "$(
cat <<EOF
{
  "Status" : "$status",
  "UniqueId" : "$puppet_role",
  "Data" : "$data",
  "Reason" : "$reason"
}
EOF
)" $signal_url
fi
